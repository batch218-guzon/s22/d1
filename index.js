console.log("Hello World!");

// Array methods

/*
    -Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
    -Array can be either mutated or iterated
        - Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/

// Mutator Methods
// functions / methods that mutate or change an array
// These manipulates the originaal array performing task such as adding and removing elements

console.log("==Mutator Methods==")
console.log("-----------------------------------------")
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"]

// push()
/*
	-Adds an element in the end of an array and returns new array's length
	-Syntax:
		arrayName.push[newElement];
*/

console.log("=>push()");
console.log("fruits array: ");
console.log(fruits);

// fruits[fruits.length] = "Mango";
let fruitsLength = fruits.push("Mango");
console.log('Size/length of fruits array: ' + fruits.length)
console.log('Mutated array from push("Mango"): ');
console.log(fruits)

fruits.push("Avocado", "Guava");
// console.log(fruits.push("Avocado", "Guava")); // returns the new array's length
console.log('Mutated array from push ("Avocado", "Guava"): ')
console.log(fruits)

/*function addMultipleFruits(fruit1, fruit2, fruit3) {
	fruits.push(fruit1, fruit2, fruit3);
	console.log(fruits);
}
addMultipleFruits("Durian", "Atis", "Melon")
*/

// pop()
/*
	-Removes the last element in an array and returns
*/

console.log("-----------------------------------------")
console.log("=>pop()");

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from pop method: ");
console.log(fruits);

// unshift
/*
    - Add one or more elements at the beginning of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/

console.log("-----------------------------------------")
console.log("=>unshift");

fruits.unshift("Lime", "Banana");
console.log('Mutated array from unshift("Lime", "Banana"): ')
console.log(fruits)

// shift()
/*
	-Removes an element at the beginning of an array AND returns the removed statement
*/

console.log("-----------------------------------------")
console.log("=>shift()");
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);

// splice()
/*
    - simulatanously removes an element from a specified index number and adds new elements.
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

console.log("-----------------------------------------")
console.log("=>splice()");

			 //s 1        2
	//0			1         2
// ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']
			 //s //d  //elements to be added
fruits.splice(1, 2, "Lime", "Cherry", "Tomato");
console.log("Mutated array from splice method: ");
console.log(fruits);

// we can also use splice to remove an element/s
// to do that our syntax should be:
/*
-syntax:
	arrayName.splice(startingIndex, deletCount);
*/
/*fruits.splice(2,4);
console.log(fruits)*/

// sort();
/*
    -Rearranges the array elements in alphanumeric order
    -Syntax:
        - arrayName.sort();
*/

console.log("-----------------------------------------")
console.log("=>sort()");
fruits.sort()
console.log("Mutated array from sort method: ")
console.log(fruits);

// reverse();
/*
    - Reverse the order of array elements
    - Syntax:
        arrayName.reverse();
*/

console.log("-----------------------------------------")
console.log("=>reverse()");
fruits.reverse()
console.log("Mutated array from reverse method: ")
console.log(fruits);

// Non-mutator
/*
	-Non-mutator methods are functions that do not modify or change an array after they're created.
*/

console.log("-----------");
console.log("-----------");
console.log("==Non-Mutator Methods==");
console.log("-----------");

				 //0     1     2      3     4     5     6     7
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
/*
    - Returns the index of the first matching element found in an array.
    - If no match was found, the result will be -1.
    - The search process will be done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/

console.log("=>indexOf()");
let firstIndex = countries.indexOf("PH")
console.log("Result of indexOf('PH'): " + firstIndex)
							// "PH" - element to add
							// 2 - starting index where to search
// let firstIndex = countries.indexOf("PH", 2); //with a specified index start
// console.log(firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf('PH'): " + invalidCountry);

// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array.
    - The search from process will be done from the last element proceeding to the first element.
    - Syntax:
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
*/


console.log("-----------");
console.log("=>lastIndexOf()");
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf('PH'): " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log('Result of lastIndexOf("PH", 4): ' + lastIndexStart);

// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/
console.log("-----------");
console.log("=>slice()");

console.log("Original countries array:");
console.log(countries);

// Slicing off elements from specified index to the last element

let sliceArrayA = countries.slice(2);
console.log("Result from slice(2):");
console.log(sliceArrayA);

// Slicing off element from specified index to another index. But the specified last index is not included in the return.
let sliceArrayB = countries.slice(2, 5); //2 -> 4 // ends with the last declared index minus one
console.log("Result from slice(2, 5):");
console.log(sliceArrayB);

let sliceArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(sliceArrayC);

// toString
/*
	- Returns an array as a string, separated by commas.
	- Syntax
		arrayName.toString();
*/

console.log("-----------");
console.log("=>toString()");

// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);
// console.log(stringArray[0];
console.log(typeof stringArray); //to check if the array is converted to string.

// concat
/*
    - combines two arrays AND returns the combined result.
    - Syntax:
        arrayA.concat(arrayB);
        arrayA.concat(element);
*/

console.log("-------------");
console.log("=>concat()"); //concat is a shortcut of concatenation

let tasksArrayA = ["drink html","eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

console.log("taskArrayA: ");
console.log(tasksArrayA)
console.log("taskArrayB: ");
console.log(tasksArrayB)
console.log("taskArrayC: ");
console.log(tasksArrayC);

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concatenating taskArrayA and taskArrayB");
console.log(tasks);

// Combining multiple arrays
console.log("Result from concatenating all tasks arrays: ");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// Combining array with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concatenating taskArrayA with other elements");
console.log(combinedTasks);

// join()
/*
    - Returns an array as a string separated by specified separator string.
    - Syntax"
        arrayName.join("separatorString");
*/

console.log("-------------");
console.log("=>join()")
let users = ["Josephine", "Enola", "John Snow", "Gambit"];
console.log(users);

console.log(users.join())
console.log(users.join(""));
console.log(users.join(" "));
console.log(users.join(" - "));

// Iteration Methods

/*
    - Similar to a for loop that iterates on each array element.
    -  For each item in the array, the anonymous function passed in the forEach() method will be run.
    - The anonymous function is able to received the current item being iterated or loop over.
    - Variable names for arrays are written in plural form of the data stored.
    - It's common practice to use the singular form of the array content for parameter names used in array loops.
    - forEach() does not return anything.
    - Syntax:
        arrayName.forEach(function(indivElement){
            //statement/code block.
        })
*/
    console.log("-------------");
    console.log("Output using for loop")
    // ["drink html","eat javascript", "inhale css", "breathe sass", "get git", "be node"];
        //initialization     //condition    //change of value
    for(let index = 0; index < allTasks.length; index++) {
        console.log(allTasks[index]);
    }

    // index = 0, index < allTask.length); => t
        //console.log(allTasks[index]); => "drink html"
        // index++ => 2
    // index = 1, index < allTask.length); => t
        //console.log(allTasks[index]); => "eat javascript"
        // index++ => 2
    // index = 2, index < allTask.length); => t
        //console.log(allTasks[index]); => "inhale css"
        // index++ => 3
    // index = 3, index < allTask.length; => t
        //console.log(allTasks[index]); => "breathe sass"
        // index++ => 4
    // index = 4, index < allTask.length); => t
        //console.log(allTasks[index]); => "get git"
        // index++ => 5
    // index = 5, index < allTask.length); => t
        //console.log(allTasks[index]); => "be node"
        // index++ => 6

    // index = 6, index < allTask.length); => f
        //exit the loop


    console.log("-------------");
    console.log("Output using forEach")
    let filteredTasks = []; //store in this variable all task names with characters greater than 10.

                // "task" - anonymous function "parameter" represents each element of the array to be iterated.
    allTasks.forEach(function(task) {
        console.log(task);


        // If the element/string's length is greater than 10 characters
        if(task.length > 10) {
          filteredTasks.push(task);
        }
    });
console.log("Result of filtered tasks: ")
console.log(filteredTasks)

// map()
/*
    - Iterates on each element and returns new array with different values depending on the result of the function's operation.
    - This is useful for performing tasks where mutating/changing the elements required.

    -Syntax
        let/const resultArray = arrayName.map(function(indivElement));
*/
console.log("-----------");
console.log("=>map()");
let numbers = [1, 2, 3, 4, 5];

let numbersMap = numbers.map(function(number) {
    return number * number;
})

console.log("Original Array: ");
console.log(numbers)

console.log("Results of the map method: ");
console.log(numbersMap)

// map() vs forEach()
console.log("-------------")
console.log("map() vs forEach()")
let numberforEach = numbers.forEach(function(number) {
    // console.log(number*number);
    return number * number;
})

console.log(numberforEach);

// every()
/*
    - checks if ALL elements in an array meet the given condition. 
    - This is useful for validation data stored in arrays especially when dealing with large amounts of data.
    - Returns a true value if all elements meet the condition and false if otherwise.
    -Syntax:
        let/const = resutlArray = arrayName.every(function(indivual element){
            return expression/condition;
        })
*/

    console.log("-----------");
    console.log("=>every()");
    numbers = [ 5, 7, 9, 1, 5];
    // - checks if ALL elements in an array meet the given condition. 
    let allValid = numbers.every(function(number){
        return (number < 3);
    });

    console.log("Result of every method: ")
    console.log(allValid);

// some()
/*
    - Check if at least one element in the array meets the given condition.
    - Returns a true if  at least one elements meets the conditon and false otherwise.
    -Syntax:
        let/const resultArray = arrayName.some(function(individElement){
            return expression/condition;
        })
*/

console.log("-----------");
console.log("=>some()");
// - Check if at least one element in the array meets the given condition.
let someValid = numbers.some(function(number){
    return (number < 2);
})
console.log("Result of some method: ");
console.log(someValid);

// Combining the returned results from the every/some metho may be used in other statements to perform consecutive result.
if(someValid){
    console.log("Some numbers in the array are greater than 2.");
}

// filter()
/*
    - Return a new array that contaions elements which meets the given condition.
    - Return an empty array if no elements were found.
    - Useful for filtering array elements with given condition and shortens the syntax.
    - Syntax:
        let/const resultArray = arrayName.filter(function(indivElement){
            return expression/condition:
        })
*/

console.log("-----------");
console.log("=>filter()");
numbers = [1, 2, 3, 4, 5];
        
                //filterValid - variable where store the result
                //arayName
let filterValid = numbers.filter(function(number){
    return (number < 3);
});

console.log("Result of filter method:");
console.log(filterValid);

// No elements found
let nothingFound = numbers.filter(function(number){
    return number == 0;
})
console.log("Result of filter method: ");
console.log(nothingFound);

filteredTasks = allTasks.filter(function(task) {
    return (task.length > 10);
});

// every - returns true if all satisfies the condition
// some - returns true if some (even just one) statisfies the condition
// filter - returns the element that satisfies the condition



// includes()
/*
    - checks if the argument passed can be found in the array.
    - it returns a boolean which can be saved in a variable
    - Syntax:
        let/const variableName = arrayName.include(<argumentToFind>);
*/

// efficient for searching inside an array

console.log("-----------");
console.log("=>includes()");
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound = products.includes("Mouse");
console.log("Result of includes method: ");
console.log(productFound); //returns true

let productNotFound = products.includes("Headset");
console.log("Result of includes method:");
console.log(productNotFound); //returns false

// Method chaining
    // The result of the inner method is used in the outer method until all "chained" methods have been resolved. 

    console.log("-----------");
    console.log("[Method chaining]");
    //products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

    //filter products array, display only products with letter a
    let filteredProducts = products.filter(function(product){
                //keyboard
        return product.toLowerCase().includes('a');
    })
console.log("Result of the chained method:");
        console.log(filteredProducts);

// reduce()
/*
    - Evaluates elements from left and right and returns/reduces the array into a single value.
    - Syntax:
        let/const resultVariable = arrayName.reduce(function(accumulator, currentValue){
            return expression/operation
        });
            - accumulator parameter stores the result for every loop.
            - currentValue parameter refers to the current/next element in the array that is evaluated in each iteration of the loop. 
*/

console.log("-----------");
console.log("=>reduce");
let i = 0;

numbers = [1, 2, 3, 4, 5];

let reducedArray = numbers.reduce(function(acc, cur){
    // console.warn("current iteration " + ++i);
    console.log("accumulator: " +acc); //1  //3  //6  //10
    console.log("current value:" + cur); //2  //3  //4  //5

    // The operation or expression to reduce the array into a single value.
    return acc + cur;
}); 

console.log("Result of reduce method: " + reducedArray);